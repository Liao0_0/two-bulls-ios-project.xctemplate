//
//  JSONIntToStringTransform.swift
//  iOSProjectTemplate
//
//  Created by Kexin Liao on 22/11/18.
//  Copyright © 2018 Two Bulls. All rights reserved.
//

import Foundation
import ObjectMapper

class JSONIntToStringTransform: TransformType {
    
    typealias Object = String
    typealias JSON = Int64
    
    init() {}
    func transformFromJSON(_ value: Any?) -> String? {
        if let intValue = value as? Int64 {
            return String(intValue)
        }
        return value as? String ?? nil
    }
    
    func transformToJSON(_ value: String?) -> Int64? {
        if let stringValue = value {
            return Int64(stringValue)
        }
        return nil
    }
}
