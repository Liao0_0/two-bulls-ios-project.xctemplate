# iOS Project Template Documentation

- [Steps to import the Template](#steps-to-import-the-template)
- [The Template currently includes the support for](#the-template-currently-includes-the-support-for)
- [Steps after creating a new iOS project using the Template](#steps-after-creating-a-new-ios-project-using-the-template)
- [References](#reference)

### Steps to import the Template
1. Create a folder named “Templates” under the “~/Library/Developer/Xcode/” directory (if not yet exists)
    - `mkdir ~/Library/Developer/Xcode/Templates`
1. Clone the Template from https://bitbucket.org/Liao0_0/two-bulls-ios-project.xctemplate/src/master/ into the “~/Library/Developer/Xcode/Templates” directory (the extension of the Template folder matters)
1. Restart Xcode and create a new project, you will see the template

### The Template currently includes the support for
1. Cartfile (for Carthage)
	- ObjectMapper
1. Buddybuild_carthage_command.sh
1. Some common classes
	- A JSONIntToStringTransform class file under the Utility group

### Steps after creating a new iOS project using the Template
1. Build dependencies via Carthage
	- `carthage update --platform iOS`
	- Or run the “buddybuild_carthage_command.sh”
1. Link the built frameworks to the project
	- To “Linked Frameworks and Libraries”
	- You may need to [create a Run Script to copy frameworks](https://github.com/Carthage/Carthage#if-youre-building-for-ios-tvos-or-watchos) to address the [library-not-loaded / image-not-found issue](https://stackoverflow.com/questions/28244468/dyld-library-not-loaded-rpath-with-ios8)

### References
https://useyourloaf.com/blog/creating-custom-xcode-project-templates/

https://stackoverflow.com/questions/37664281/how-to-create-a-project-template

https://stackoverflow.com/questions/15558953/xcode-project-template-add-file-to-the-project-group
